import { ICarShop } from './../interfaces/car-shop';
// import { ICar } from './car';
import { ObjectId } from 'mongodb';

export class Shop implements ICarShop {
    _id?: ObjectId;
    imgShop: '';
    name: '';
    cars: ObjectId[];
}