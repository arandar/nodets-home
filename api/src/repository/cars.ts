// import { Car } from './../models/classes/car';
import { Collections } from '../models/enums/collections';
import { ObjectId } from 'mongodb';
import { MongoService } from 'typespring';
import { Repository } from 'typespring';
// import { IClient } from '../models/interfaces/clients';
// import { Client } from '../models/classes/client';
import { ICar } from '../models/interfaces/car';
// import { Collection } from 'ts-migrator';

@Repository()
export class CarsRepository {

    constructor(
        private mongo: MongoService,
    ) { }

    getCars(): Promise<ICar[]> {
        // console.log('from repo');
        return this.mongo.collection(Collections.Cars).find({}).toArray();
    }

    // async getClientClients(): Promise<IClient[]> {
    //     return this.mongo.collection(Collections.Clients).find(
    //         { isVisible: true },
    //     ).project(
    //         { isVisible: 0, _id: 0 },
    //     ).toArray();
    // }

    // первый вариант добавления с updateOne
    // async addCar(car, shop): Promise<string> {
    //     const sendCar = await this.mongo.collection(Collections.Cars).insertOne(car);
    //     console.log(sendCar.ops); // вывод всего объекта
    //     const writeCar = await this.mongo.collection(Collections.Shops).updateOne(
    //         { _id: new ObjectId(shop) },
    //         { $addToSet: { cars: sendCar.insertedId } },
    //     );
    //     assert.equal(writeCar.modifiedCount, 1, 'addCar update error'); // проверка, залилось ли что-нибудь
    //     return String(sendCar.insertedId); // возврат в виде строки нашего id машины
    // }

    // второй вариант с findAndUpdate
    async addCar(car, shop): Promise<string> {
        const sendCar = await this.mongo.collection(Collections.Cars).insertOne(car);
        console.log(sendCar.ops);
        // этот вариант отличается отсутствием необходимости проверки, т.к метод сам выводит объект
        await this.mongo.collection(Collections.Shops).findOneAndUpdate(
            { _id: new ObjectId(shop) },
            { $addToSet: { cars: sendCar.insertedId } },
            { returnOriginal: false }, // возвращает измененный объект, а не старую версию
        );
        return JSON.stringify(`${sendCar.insertedId}`);
    }

    // Первый вариант редактирования через update
    // async updateCar(car): Promise<void> {
    //     const id = car._id;
    //     delete car._id;
    // он просто меняет содержимое объекта
    //     await this.mongo.collection(Collections.Cars).updateOne({ _id: new ObjectId(id) }, { $set: { ...car } });
    // }

    // Второй вариант редактирования через replace
    async updateCar(car): Promise<void> {
        const id = car._id;
        delete car._id;
        // меняет весь объект (выбрал из-за меньшей писанины)
        await this.mongo.collection(Collections.Cars).replaceOne({ _id: new ObjectId(id) }, car);
    }

    // удаление первый вариант с передачей id магазина и машины
    // подходит для поиска по одному магазину, для нескольких нужно updateMany, хорош тем,
    // что скорость обработки гораздо меньше, чем у второго варианта
    async deleteCar(car, shop): Promise<void> {
        const deleteCar = await this.mongo.collection(Collections.Cars).findOneAndDelete({ _id: new ObjectId(car) });
        const delInShop = await this.mongo.collection(Collections.Shops).updateOne(
            { _id: new ObjectId(shop) },
            { $pull: { cars: new ObjectId(car) } },
        );
    }

    // удаление второй вариант
    // не нужно вводить id магазина, передает пустой объект, поэтому отсутствует лишний параметр,
    // да и ищет по всей коллекции
    // плюс - удаляет везде, если находит, минус - очень долго будет грузить, если у нас большая база
    // async deleteCar(car): Promise<void> {
    //     const deleteCar = await this.mongo.collection(Collections.Cars).findOneAndDelete({ _id: new ObjectId(car) });
    //     const delInShop = await this.mongo.collection(Collections.Shops).updateOne(
    //         {}, // здесь пустой объект
    //         { $pull: { cars: new ObjectId(car) } },
    //     );
    // }
}