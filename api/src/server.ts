import { ConfigService } from './services/convict.service';
import { Server, bootstrap, LoggerFactory } from 'typespring';
import { CarsController } from './controllers/cars';
import { CarsRepository } from './repository/cars';
import { ShopsRepository } from './repository/shops';
import { AppInitializerService } from './services/app-initializer.service';
import { ShopsController } from './controllers/shops';

@Server({
    port: PORT,
    cors: true,
    services: [
        ConfigService,
        LoggerFactory,
        AppInitializerService,

    ],
    controllers: [
        CarsController,
        ShopsController,
    ],
    repositories: [
        CarsRepository,
        ShopsRepository,
    ],
})
class AppServer { }

bootstrap(AppServer);