import { CarsEffects } from './store/cars/cars.effect';
import { environment } from './../environments/environment';
import { HomeModule } from './pages/home/home.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditorModule } from './pages/editor/editor.module';
import { NavigationModule } from './pages/navigation/navigation.module';
import { ContactsModule } from './pages/contacts/contacts.module';
import { CarInfoModule } from './pages/car-info/carInfo.module';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appReducers, metaReducers } from './store/index';
import { EffectsModule } from '@ngrx/effects';
import { CarShopsEffects } from './store/car-shops/car-shops.effect';
import { ShopModule } from './pages/shop/shop.module';

@NgModule({
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		BrowserAnimationsModule,
		EditorModule,
		NavigationModule,
		ContactsModule,
		CarInfoModule,
		HomeModule,
		ShopModule,
		StoreModule.forRoot(appReducers, { metaReducers }),
		StoreDevtoolsModule.instrument({
			maxAge: 25,
			logOnly: environment.production,
		}),
		EffectsModule.forRoot([
			CarShopsEffects,
			CarsEffects,
		]),
	],
	declarations: [
		AppComponent,
	],
	bootstrap: [AppComponent]
})
export class AppModule { }