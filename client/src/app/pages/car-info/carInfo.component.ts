import { getCarsStoreState } from './../../store/cars/cars.reducer';
import { getCarShopsStoreState } from './../../store/car-shops/car-shops.reducer';
import { AppState } from './../../store/index';
import { ICar } from './../../shared/models/interfaces/car';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { Location } from '@angular/common';

@Component({
    selector: 'car-info',
    templateUrl: './carInfo.component.html',
    styleUrls: ['./carInfo.component.scss']
})

export class CarInfoComponent implements OnInit, OnDestroy {
    data$: Observable<ICar>;
    subscription: Subscription;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>,
        private location: Location,
    ) { }

    ngOnInit(): void {
        // подписываемся на изменения параметров из строки браузера. в ней имеются два свойства
        // свойства carId и shopId, которые есть в строке браузера. эти свойства задаются в app.routing (см. 18 строка)
        // проверить можно "subscribe((p) => {console.log(p, 'ggg')}"
        this.subscription = this.activatedRoute.params.subscribe((p) => {
            // метод pipe преобразует ???асинхронные??? данные в "нормальные"
            // при выводе в консоль оно выдаст нам наш стор
            this.data$ = this.store.select(getCarsStoreState).pipe(
                // console.log(s) выведет нам объект с двумя своствами: флагом isCarShopsLoading и массивом shops
                // (эти свойства мы указывали в reducer'e в классе CarShopsState )
                // далее, мы выбираем параметр и присваиваем ему свойство shops (массив shops)
                // делаем проверку, если ли у параметра s свойство shops.
                // объявляем две константы shop и car. в них через метод find сравнимается 
                // параметр _id нашего массива shops и параметром из строки браузера
                // если верно, то константа shop будет равна массиву shops 
                // в константе car проверяется, есть ли массив shop === shops, есть ли в нём свойство cars и 
                // равно ли _id машины с нашим параметром id машинры из браузерной строки
                // для прогона можно использовать - console.log(s, 'dsfg') и console.log(shop, 'yyy') 

				map(s => {
                    const car = s && s.cars && s.cars.find(c => c._id == p.carId);
                    return car;
                })
            );
        })
    }

    ngOnDestroy(): void {
        if (this.subscription) this.subscription.unsubscribe();
    }

    goEdit(): void {
		this.subscription = this.activatedRoute.params.subscribe((p) => {
			this.router.navigate([`/edit/${p.shopId}/${p.carId}`]);
		});
    }

    goHome(): void {
        this.location.back();
    }
}