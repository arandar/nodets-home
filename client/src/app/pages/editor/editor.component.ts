import { getCarsStoreState } from './../../store/cars/cars.reducer';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/index';
import { getCarShopsStoreState } from '../../store/car-shops/car-shops.reducer';
import { ICar } from '../../shared/models/interfaces/car';
import { map } from 'rxjs/operators';
// import { DataService } from '../../shared/services/data.service';
import { AddCarAction } from '../../store/cars/cars.action';

@Component({
    selector: 'car-test-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit, OnDestroy {

    editorMode: boolean = false;
    carForm: FormGroup;
    subscription: Subscription;
    data$: Observable<ICar>

    constructor(
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private store: Store<AppState>,
        // private dataService: DataService
    ) { }

    ngOnInit(): void {
        // подписка на параметры передаваемые в url
        this.subscription = this.activatedRoute.params.subscribe((p) => {
			
            // если параметры переданы, то включается режим редактирования, которые изменяет название формы
            if (p.shopId && p.carId) {
				this.editorMode = true;
            } else {
                this.initForm();
            }
            // в переменную data$ записываются данные из store

        });
    }

    initForm(car?: ICar): void {
        if (car) {
            this.carForm = this.fb.group({
                img: [car.img, [Validators.required]],
                color: [car.color, [Validators.required]],
                model: [car.model, [Validators.required]],
                price: [car.price, [Validators.required]],
                brand: [car.brand, [Validators.required]],
                age: [car.age, [Validators.required]],
                description: [car.description, [Validators.required]],
            });
        } else {
            this.carForm = this.fb.group({
                img: ["", [Validators.required]],
                color: ["", [Validators.required]],
                model: ["", [Validators.required]],
                price: ["", [Validators.required]],
                brand: ["", [Validators.required]],
                age: ["", [Validators.required]],
                description: ["", [Validators.required]],
            })
        }

    }

    onSubmit(): void {
        if (!this.carForm.valid) return;
        if (this.editorMode) {
            this.editCar(this.carForm.value);
        } else {
            this.addCar(this.carForm.value);
        }
        this.router.navigate(['/home']);
    }

	
    addCar(carId): void {
        this.store.dispatch(new AddCarAction(this.activatedRoute.params['value'].shopId, carId));
    }

    editCar(formValue): void {
        const routeParams = this.activatedRoute.params['value'];
        // передаем в сервис значения из роутера и объект значений из формы
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
