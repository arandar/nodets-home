//Artem
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './navigation.component';
import { MaterialModule } from '../../shared/modules/material.module';
@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MaterialModule
    ],

    declarations: [
        NavigationComponent,
    ],

    exports: [
        NavigationComponent,
    ],
})
export class NavigationModule { }
    