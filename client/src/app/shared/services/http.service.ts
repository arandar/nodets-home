import { ICarShop } from './../models/interfaces/car-shop';
import { ICar } from './../models/interfaces/car';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class HttpService {
	constructor(private http: HttpClient) { }

	get apiUrl(): string {
		return 'http://localhost:3000/api';
	}

	getCarShops(): Observable<ICarShop[]> {
		return this.http.get<ICarShop[]>(`${this.apiUrl}/shops`);
	}

	getCars(): Observable<ICar[]> {
		return this.http.get<ICar[]>(`${this.apiUrl}/cars`);
	}

	// post запрос на сервер 
	addCar(shopId, carId): any {
		const id = { car: { ...carId }, "shopId": shopId };
		const req = this.http.post<any>(`${ this.apiUrl }/cars`, id);
		return req;
	}
}
