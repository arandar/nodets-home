import { ICarShop } from './../../shared/models/interfaces/car-shop';
import { Action } from '@ngrx/store';
import { ICar } from '../../shared/models/interfaces/car';

export enum CarShopsTypes {
    LoadCarShops = '[CAR-SHOP] load car shops [...]',
    LoadCarShopsSuccess = '[CAR-SHOP] load car shops [SUCCESS]',
	LoadCarShopsError = '[CAR-SHOP] load car shops [ERROR]',
	AddCarToShop = '[CAR-SHOP] add car to shop [...]',
    // DeleteShop = 'DELETE_SHOP',
    // DeleteCar = 'DELETE_CAR',
    // EditCar = 'EDIT_CAR',
    // AddCar = 'ADD_CAR',
}

// LOAD CAR SHOPS
export class LoadCarShopsAction implements Action {
    readonly type = CarShopsTypes.LoadCarShops;
}

export class LoadCarShopsSuccessAction implements Action {
    readonly type = CarShopsTypes.LoadCarShopsSuccess;
    constructor(public payload: ICarShop[]) { }
}

export class LoadCarShopsErrorAction implements Action {
    readonly type = CarShopsTypes.LoadCarShopsError;
    constructor(public payload: any) { }
}

export class AddCarToShopAction implements Action {
    readonly type = CarShopsTypes.AddCarToShop;
    constructor(public shopId: any, public carId: any) { }

}


// тут экспортируются типы
export type CarShopsActions
    = LoadCarShopsAction
    | LoadCarShopsSuccessAction
	| LoadCarShopsErrorAction
	| AddCarToShopAction
    // | DeleteShopAction
    // | DeleteCarAction
    // | EditCarAction
    // | AddCarAction
    ;