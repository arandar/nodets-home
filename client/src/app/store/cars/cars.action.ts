import { Action } from '@ngrx/store';
import { ICar } from '../../shared/models/interfaces/car';

export enum CarsTypes {
    LoadCars = '[CAR] load cars [...]',
    LoadCarsSuccess = '[CAR] load cars [SUCCESS]',
    LoadCarsError = '[CAR] load cars [ERROR]',
    AddCar = '[CAR-SHOP] add car [...]',
    AddCarSuccess = '[CAR-SHOP] add car [SUCCESS]',
    AddCarError = '[CAR-SHOP] add car [ERROR]',
}

// LOAD CAR SHOPS
export class LoadCarsAction implements Action {
    readonly type = CarsTypes.LoadCars;
}

export class LoadCarsSuccessAction implements Action {
    readonly type = CarsTypes.LoadCarsSuccess;
    constructor(public payload: ICar[]) { }
}

export class LoadCarsErrorAction implements Action {
    readonly type = CarsTypes.LoadCarsError;
    constructor(public payload: any) { }
}

// ADD CAR
export class AddCarAction implements Action {
	readonly type = CarsTypes.AddCar;
	constructor(public shopId, public carId) {}
}

export class AddCarSuccessAction implements Action {
	readonly type = CarsTypes.AddCarSuccess;
	payload: any;
	constructor(payload: {shopId: string, car: ICar}) {}
}

export class AddCarErrorAction implements Action {
	readonly type = CarsTypes.AddCarError;
	constructor(public payload: any) {}
}


// тут экспортируются типы
export type CarsActions
    = LoadCarsAction
    | LoadCarsSuccessAction
    | LoadCarsErrorAction
    | AddCarAction
    | AddCarSuccessAction
    | AddCarErrorAction
    ;