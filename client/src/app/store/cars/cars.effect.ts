import { AddCarToShopAction } from './../car-shops/car-shops.action';
import { HttpService } from './../../shared/services/http.service';
import { switchMap, map, tap, catchError, mergeMap } from 'rxjs/operators';
import { of, Observable, empty } from 'rxjs';
import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { CarsActions, CarsTypes, LoadCarsAction, LoadCarsSuccessAction, LoadCarsErrorAction, AddCarAction, AddCarSuccessAction, AddCarErrorAction } from './cars.action';

@Injectable()
export class CarsEffects {

    @Effect()
    loadCars$: Observable<CarsActions> = this.actions$.pipe(
        ofType<LoadCarsAction>(CarsTypes.LoadCars),
        switchMap(() => {
            return this.http.getCars().pipe(
                map((res: any) => new LoadCarsSuccessAction(res)),
                catchError(error => of(new LoadCarsErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<CarsActions> = this.actions$.pipe(
        ofType<LoadCarsErrorAction>
            (CarsTypes.LoadCarsError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
            // this.errorService.toConsole(e);
        }),
	);
	
	@Effect()
	addCar$: Observable<CarsActions> = this.actions$.pipe(
		ofType<AddCarAction>(CarsTypes.AddCar),
		switchMap((value) => {
			return this.http.addCar(value.shopId, value.carId).pipe(
				mergeMap((res: any) => {
					return of(new AddCarSuccessAction({ shopId: value.shopId, car: { ...value.carId, _id: res } }),
						new AddCarToShopAction(value.shopId, res));
				}),
				catchError(err => of(new AddCarErrorAction(err))),
			);
		}),
	);

	@Effect({ dispatch: false })
	onAddError$: Observable<CarsActions> = this.actions$.pipe(
		ofType<AddCarErrorAction>
		(CarsTypes.AddCarError),
		map(action => action.payload),
		tap(e => {
			console.error(e);
			
		}),
	);

    constructor(
        private actions$: Actions,
        private http: HttpService,
    ) { }
}