// import { ICarShop } from './../../shared/models/interfaces/car-shop';
import { CarsActions, CarsTypes } from './cars.action';
import { createFeatureSelector } from '@ngrx/store';
import { ICar } from '../../shared/models/interfaces/car';

export class CarsState {
    cars: ICar[];
    isCarsLoading: boolean = true;
}

export function reducer(state = new CarsState(), action: CarsActions): CarsState {
    switch (action.type) {
        // LOAD CARS
        case CarsTypes.LoadCars:
            return {
                ...state,
                isCarsLoading: true,
            };
        case CarsTypes.LoadCarsSuccess:
            return {
                ...state,
                cars: action.payload,
                isCarsLoading: false,
            };
        case CarsTypes.LoadCarsError:
            return {
                ...state,
                isCarsLoading: false,
                cars: [],
            };

		// ADD CARS
		case CarsTypes.AddCar:
			return {
				...state,
			};

		case CarsTypes.AddCarSuccess:
			return {
				...state,
				cars: [
					...state.cars,
					action.payload.car
				],
			};

		case CarsTypes.AddCarError:
			return {
				...state,
			};

        default: {
            return state;
        }
    }
}

export const getCarsStoreState = createFeatureSelector<CarsState>('cars');